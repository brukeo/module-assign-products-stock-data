<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\AssignProductsStockData\Helper;

class Constants
{

    const ASSIGN_PRODUCTS_TO_STOCK_DATA_COMMAND_NAME = 'brukeo:products:assign_to_stock_data';
    const ASSIGN_PRODUCTS_TO_STOCK_DATA_COMMAND_DESCRIPTION = 'Assign products to stock data.';
    const ASSIGN_PRODUCTS_TO_STOCK_DATA_UPDATE_URL_KEY_OPTION_NAME = 'update_url_key';
    const ASSIGN_PRODUCTS_TO_STOCK_DATA_UPDATE_URL_KEY_OPTION_SHORTCUT = 'u';
    const ASSIGN_PRODUCTS_TO_STOCK_DATA_UPDATE_URL_KEY_OPTION_DESCRIPTION = 'Update url key by adding SKU to it (1 = yes)';

}
