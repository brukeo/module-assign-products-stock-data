<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

namespace Brukeo\AssignProductsStockData\Console\Command;

class AssignProductsStockData extends \Symfony\Component\Console\Command\Command
{

    protected \Magento\Framework\App\State $appState;
    protected \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory;

    public function __construct
    (
        \Magento\Framework\App\State $appState,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        string $name = null
    )
    {
        $this->appState = $appState;
        $this->productCollectionFactory = $productCollectionFactory;

        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this->setName(\Brukeo\AssignProductsStockData\Helper\Constants::ASSIGN_PRODUCTS_TO_STOCK_DATA_COMMAND_NAME);
        $this->setDescription(\Brukeo\AssignProductsStockData\Helper\Constants::ASSIGN_PRODUCTS_TO_STOCK_DATA_COMMAND_DESCRIPTION);
        $this->addOption(
            \Brukeo\AssignProductsStockData\Helper\Constants::ASSIGN_PRODUCTS_TO_STOCK_DATA_UPDATE_URL_KEY_OPTION_NAME,
            \Brukeo\AssignProductsStockData\Helper\Constants::ASSIGN_PRODUCTS_TO_STOCK_DATA_UPDATE_URL_KEY_OPTION_SHORTCUT,
            \Symfony\Component\Console\Input\InputOption::VALUE_REQUIRED,
            \Brukeo\AssignProductsStockData\Helper\Constants::ASSIGN_PRODUCTS_TO_STOCK_DATA_UPDATE_URL_KEY_OPTION_DESCRIPTION
        );

        parent::configure();
    }

    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ): int
    {
        $this->appState->setAreaCode('frontend');

        $updateUrlKey = (int) $input->getOption(\Brukeo\AssignProductsStockData\Helper\Constants::ASSIGN_PRODUCTS_TO_STOCK_DATA_UPDATE_URL_KEY_OPTION_NAME);
        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addAttributeToFilter('type_id', [
            'eq' => 'simple'
        ]);
        $items = $productCollection->getSize();
        $i = 1;
        /** @var \Magento\Catalog\Model\Product $product */
        foreach ($productCollection as $product) {
            if (!$product->getSku()) {
                $output->writeln(sprintf(
                    '<error>%s</error>',
                    __('Product with ID %1 have empty SKU.', $product->getId())
                ));
                $i++;
                continue;
            }

            $output->writeln(sprintf(
                '<info>%s</info>',
                __('(%1-%2) Assigning stock source to product - %3.', $i, $items, $product->getSku())
            ));

            $product->setStockData([
                'qty' => 999,
                'is_in_stock' => 1
            ]);

            if ($updateUrlKey === 1) {
                $urlKey = $product->getUrlKey();
                $urlKey = sprintf("%s-%s", $urlKey, $product->getSku());
                $urlKey = strtolower($urlKey);
                $product->setUrlKey($urlKey);
            }

            $product->save();

            $i++;
        }

        return 1;
    }

}
